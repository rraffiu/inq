Theory
======

.. caution::
     This is draft documentation.
.. note::

     INQ is under active development.

Total Energy
------------

The total energy in Kohn-Sham DFT is, 

.. math::
  :label: eq1

  E_{total} = E_{kin} + E_{classical}[n_{el}] -E_{ion-self-interaction} +E_{xc}[n_el],

where :math:`E_{classical}` is the classical interaction energy involving ions and electrons (ion-ion, ion-electron and electron-electron), given by
