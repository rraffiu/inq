.. inq documentation master file, created by
   sphinx-quickstart on Fri Nov  3 14:11:15 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to INQ's documentation!
===============================

.. caution::
   This is draft documentation.

.. note::

   INQ is under active development, visit `gitlab.com/NPNEQ/inq. <https://gitlab.com/NPNEQ/inq>`_

**INQ** is an engine for electronic structure calculations.
It can work in three ways, as a standalone electronic structure code, as a library to implement complex electronic structure methods, or as a proxy-app to evaluate the performance of electronic structure algorithms in high-performance computing platforms.
It concentrates on algorithms as a portability hardware layer, generic types, and flexibility of usage.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   installation
   units
   tutorials
   theory
   code

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
