Tutorials
=========

.. caution::
     This is draft documentation.
.. note::

     INQ is under active development.

This is a set of tutorials to teach you how to use Inq.
Note that the tutorial is meant to be read completely.
Unlike other code tutorials we won't give you a full code for all the tasks, you have to write your own following our instructions.
So if something doesn't work go back and read the instructions again.
To follow the tutorial you will need access to a Unix system were to compile and run Inq.
We assume you have a basic knowledge of a Unix terminal, file manipulation and access to an editor of your choice.


